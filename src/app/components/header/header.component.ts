import { Component, OnInit } from '@angular/core';
import { ToggleSidebarService } from '../toggle-sidebar.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isExpand: boolean;
  constructor(private toggle: ToggleSidebarService) {

  }

  ngOnInit(): void {
  }

  toggleMenu(status: boolean) {
    this.toggle.isExpand.next(status);
  };

}
