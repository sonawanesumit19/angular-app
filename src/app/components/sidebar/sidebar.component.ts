import { Component, OnInit } from '@angular/core';
import { ToggleSidebarService } from '../toggle-sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  isExpand:boolean;
  constructor(private toggle: ToggleSidebarService) {
    this.toggle.isExpand.subscribe(status => {
      this.isExpand = status
      console.log('sidebar ' + this.isExpand);
    })
  }

  ngOnInit(): void {
  }

}
