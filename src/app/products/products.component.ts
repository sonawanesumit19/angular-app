import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  productList = [
    {
      ProductName: 'Product 1',
      ProductImage: 'https://rukminim1.flixcart.com/image/495/594/kflftzk0-0/t-shirt/p/d/v/l-hm-1001-mustard-black-helmont-original-imafwyfb2tqmjdhw.jpeg?q=50',
      ProductDesc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      ProductPrice: '899',
      ProductLink: 'Product Link'
    },
    {
      ProductName: 'Product 2',
      ProductImage: 'https://rukminim1.flixcart.com/image/495/594/kflftzk0-0/t-shirt/5/x/1/s-rh-rechnc-blk-rockhard-original-imafwyf9eps9evrv.jpeg?q=50',
      ProductDesc: 'Phasellus vitae nibh tempor, cursus purus in, accumsan tortor',
      ProductPrice: '699',
      ProductLink: 'Product Link'
    },
    {
      ProductName: 'Product 3',
      ProductImage: 'https://rukminim1.flixcart.com/image/495/594/kfoapow0-0/t-shirt/c/u/u/s-kd275282-kay-dee-original-imafw2kdgayvt3bz.jpeg?q=50',
      ProductDesc: 'Aliquam id magna quis sapien mattis cursus nec eu ipsum',
      ProductPrice: '999',
      ProductLink: 'Product Link'
    },
    {
      ProductName: 'Product 4',
      ProductImage: 'https://rukminim1.flixcart.com/image/495/594/kfoapow0-0/t-shirt/y/y/y/l-kd286287-kay-dee-original-imafw2jfesgdmns6.jpeg?q=50',
      ProductDesc: 'Etiam ut quam scelerisque, egestas dui vel, auctor mauris',
      ProductPrice: '799',
      ProductLink: 'Product Link'
    }
  ]
  
  constructor() { }

  ngOnInit(): void {
  }

}
