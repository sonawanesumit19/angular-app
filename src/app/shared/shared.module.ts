import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { materialModule } from './material.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    materialModule
  ],
  exports: [
    materialModule
  ]
})
export class SharedModule { }
